#Readme
This is a sample project to showcase content providers.

This demo makes of [EventBus](https://github.com/greenrobot/EventBus), [Android-Universal-Image-Loader](https://github.com/nostra13/Android-Universal-Image-Loader), the Support Library and ActionBarCompat. Thus you have to add those to your project.

Note: You might have to clean your project to get rid of Lint warnings and errors.


##Most important classes
The sample is named CPSample but apart from the CPSampleActivity most classes contain LentItems as part of their name. That's because this sample deals with lent items to illustrate the usage of content providers.

The initial starting acvtivity is `CPSampleActivity.java`. Most logic though is contained
in fragments.

See `LentItemListFragment.java` for the fragment displaying the list of lent items.
The fragment `LentItemFormFragment.java` deals with adding new or editing existing data.
The fragment `LentItemDisplayFragment.java`deals with displaying an existing item.
All Fragments make use of Loaders to load the data to display (if there _is_ anything to display).
You can find the content provider itself in the class `LentItemsProvider.java`.

The sample uses an IntentService whenever the underlying data shall be changed. This service
makes it possible to keep update operations off the UI thread. See `LentItemService.java` for details.

This tutorial doesn't use batch updates. That's part of a separate sample app.


##Relevant Blogposts on [Grokking Android](http://www.grokkingandroid.com/)
[Writing your own Content provider](http://www.grokkingandroid.com/wordpress/android-tutorial-writing-your-own-content-provider/)

[Using Content Providers](http://www.grokkingandroid.com/wordpress/android-tutorial-writing-your-own-content-provider/)

[Content Provider Basics](http://www.grokkingandroid.com/wordpress/android-tutorial-content-provider-basics/)

[How to Use Loaders in Android](http://www.grokkingandroid.com/using-loaders-in-android/)


##Warning: Generated codebase

**Please note:** I have created the core of this sample using a custom generator. I'm going to publish many samples when writing blog posts and want to keep my work for those as focused as possible. The generator helps me achieve this even if it means that some code might seem a bit odd. I always refer to the relevant classes at the beginning of this README file.

**tl;dr:** Do not copy without thinking - should go without saying, but it's better to mention it once more!


##Developed by

*Wolfram Rittmeyer* - You can contact me via:

* [Grokking Android (my blog)](http://www.grokkingandroid.com)

* [Google+](https://plus.google.com/101948439228765005787) or

* [Twitter](https://twitter.com/RittmeyerW)


##Thanks
Thanks to all my readers and blog commenters. Your feedback helps me to stay on track and to go on with projects like this.

Special thanks also to the Android crowd on G+. You're an awesome crowd. I have gained many insights by reading posts there, by following links to blog posts or by discussions on G+! 

This readme was created using [dillinger.io](http://dillinger.io). Thanks for this service.


##License
    Copyright 2013 Wolfram Rittmeyer

    Licensed under the Apache License, Version 2.0 (the "License");
    you may not use this file except in compliance with the License.
    You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

    Unless required by applicable law or agreed to in writing, software
    distributed under the License is distributed on an "AS IS" BASIS,
    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
    See the License for the specific language governing permissions and
    limitations under the License.

